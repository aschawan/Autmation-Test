import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class Testng {

    @BeforeMethod
    public void beforeMethod() {
        System.out.println("Starting Test On Chrome Browser");
    }

    @DataProvider
    public void getphonenumberdata()
    {
PhonenumberUtil.getdatafromexcel();
    }

    @Test(dataProvider="getphonenumberdata")
    public void phonenumbertest(String phonenumber)
    {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\admin\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver;
        driver.get("Sign up page");
        System.out.println("Launch sign up page");
        driver.findElement(org.openqa.selenium.By.name("phonenumber")).sendKeys("phonenumber");
        driver.findElement(org.openqa.selenium.By.name("Continue")).click();
        String testTitle = "Enter Otp page";
        String originalTitle = driver.getTitle();
        Assert.assertEquals(originalTitle, testTitle);
    }

    @DataProvider
    public void getOTPdata()
    {
        OTPUtil.getdatafromexcel();
    }
    @Test(dataProvider="getOTPdata")
    public void OTPtest(String OTP)
    {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\admin\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver;
        driver.get("Enter Otp page");
        System.out.println("Launch enter Otp page");
        driver.findElement(org.openqa.selenium.By.name("OTP")).sendKeys("OTP");
        driver.findElement(org.openqa.selenium.By.name("Continue")).click();
        String testTitle = "Enter PAN and DOB page";
        String originalTitle = driver.getTitle();
        Assert.assertEquals(originalTitle, testTitle);
    }
    @DataProvider
    public Iterator<Object[]> getPANandDOBdata()
    {
        Array<Object[]> testData = DOBandPANUtil.getdatafromexcel();
        return PANandDOBdata.iterator();
    }

    @Test(dataProvider="getPANandDOBdata")
    public void phonenumbertest(String PANnumber, DOB)
    {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\admin\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver;
        driver.get("Enter PAN and DOB page");
        System.out.println("Launch enter PAN and DOB page");
        driver.findElement(org.openqa.selenium.By.name("PANnumber")).sendKeys("PANnumber");
        driver.findElement(org.openqa.selenium.By.name("DOB")).sendKeys("DOB");
        driver.findElement(org.openqa.selenium.By.name("Continue")).click();
        String testTitle = "Enter Bank details page";
        String originalTitle = driver.getTitle();
        Assert.assertEquals(originalTitle, testTitle);
    }




    @DataProvider
    public Iterator<Object[]> getBankdetailsdata()
    {
        Array<Object[]> testData = BankdetailsUtil.getdatafromexcel();
        return Bankdetailsdata.iterator();
    }


    @Test(dataProvider="getBankdetailsdata")
    public void phonenumbertest(String IFSCcode, AccountNumber, ReenterAccountNumber, Accounttype)
    {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\admin\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver;
        driver.get("Enter Bank details page");
        System.out.println("Launch enter Bank details page");
        driver.findElement(org.openqa.selenium.By.name("IFSCcode")).sendKeys("IFSCcode");
        driver.findElement(org.openqa.selenium.By.name("AccountNumber")).sendKeys("AccountNumber");
        driver.findElement(org.openqa.selenium.By.name("ReenterAccountNumber")).sendKeys("ReenterAccountNumber");
        driver.findElement(org.openqa.selenium.By.name("Accounttype")).sendKeys("Accounttype");
        driver.findElement(org.openqa.selenium.By.name("Continue")).click();
        String testTitle = "Upload Signature page";
        String originalTitle = driver.getTitle();
        Assert.assertEquals(originalTitle, testTitle);
    }

    @Test
    public void UploadSignatureTest()
    {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\admin\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver;
        driver.get("Upload Signature page");
        System.out.println("Launch upload Signature page");
        driver.findElement(org.openqa.selenium.By.name("UploadLOGO")).click();
        driver.findElement(org.openqa.selenium.By.name("Choose from photos")).click;
        addFile.sendKeys("");
        driver.findElement(org.openqa.selenium.By.name("Continue")).click();
        String testTitle = "Signature Uploaded";
        String originalTitle = driver.getTitle();
        Assert.assertEquals(originalTitle, testTitle);
    }

    @Test
    public void UploadSignatureTest()
    {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\admin\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver;
        driver.get("Upload Signature page");
        System.out.println("Launch upload Signature page");
        driver.findElement(org.openqa.selenium.By.name("UploadLOGO")).click();
        driver.findElement(org.openqa.selenium.By.name("Choose from photos")).click;
        addFile.sendKeys("");
        driver.findElement(org.openqa.selenium.By.name("Cancel")).click();
        String testTitle = "Signature Uploaded";
        String originalTitle = driver.getTitle();
        Assert.assertEquals(originalTitle, testTitle);
    }


    @AfterMethod
    public void afterMethod() {
        driver.close();
        System.out.println("Finished Test On Chrome Browser");
    }
}